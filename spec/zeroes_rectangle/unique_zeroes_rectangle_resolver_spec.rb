require 'matrix'

RSpec.describe ZeroesRectangle::UniqueZeroesRectangleResolver do
  it "return the zeroes rectangle information when matrix is valid" do
    matrix = Matrix[[1,1,1,1,1],[1,1,1,0,0],[1,1,1,0,0],[1,1,1,1,1],[1,1,1,1,1]]
    image_matrix = ZeroesRectangle::UniqueZeroesRectangleResolver.new(matrix)
    image_matrix.print_matrix
    expect(image_matrix.zeroes_rectangle.width).to eq(2)
    expect(image_matrix.zeroes_rectangle.height).to eq(2)
    expect(image_matrix.zeroes_rectangle.start_index.x).to eq(3)
    expect(image_matrix.zeroes_rectangle.start_index.y).to eq(1)
  end

  it "return nil when matrix is valid but does't have any 0" do
    matrix = Matrix[[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1]]
    image_matrix = ZeroesRectangle::UniqueZeroesRectangleResolver.new(matrix)
    image_matrix.print_matrix
    expect(image_matrix.zeroes_rectangle).to be(nil)
  end

  it "must fail when no matrix is passed" do
    matrix = nil
    expect { ZeroesRectangle::UniqueZeroesRectangleResolver.new(matrix) }.to raise_exception(StandardError, "Error initializing object: Matrix can't be nil.")
  end

  it "must fail when matrix is invalid" do
    matrix = Matrix[[1,1,1,1,1],[1,1,1,0,0],[1,1,1,0,1],[1,1,1,1,1],[1,1,1,1,1]]
    expect { ZeroesRectangle::UniqueZeroesRectangleResolver.new(matrix) }.to raise_exception(StandardError, "Error while validating result: Not all the lines of zeroes has the same size or its coordinates differs")
  end

  it "must fail when matrix has invalid values" do
    matrix = Matrix[[2,3,1,4,1],[1,1,1,0,0],[1,1,1,0,1],[1,1,1,1,1],[1,1,1,1,1]]
    expect { ZeroesRectangle::UniqueZeroesRectangleResolver.new(matrix) }.to raise_exception(StandardError, "Error validating row content: Passed Matrix has invalid values. Only #{ZeroesRectangle::UniqueZeroesRectangleResolver::ALLOWED_VALUES} are allowed.")
  end

  it "must fail when matrix has more than one zeroes rectangle" do
    matrix = Matrix[[1,1,1,1,1],[1,1,1,0,0],[1,1,1,0,0],[1,1,1,1,1],[1,1,1,0,0]]
    expect { ZeroesRectangle::UniqueZeroesRectangleResolver.new(matrix) }.to raise_exception(StandardError, "Error while solving matrix: There is more than one zeroes rectangle on matrix.")
  end
end
