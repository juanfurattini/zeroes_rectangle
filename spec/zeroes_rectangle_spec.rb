RSpec.describe ZeroesRectangle do
  it "has a version number" do
    expect(ZeroesRectangle::VERSION).not_to be(nil)
  end

  it "has UniqueZeroesRectangleResolver class defined" do
    expect(ZeroesRectangle::UniqueZeroesRectangleResolver).not_to be(nil)
  end
end
