require 'matrix'
require 'ostruct'

module ZeroesRectangle
  class UniqueZeroesRectangleResolver

    attr_reader :zeroes_rectangle

    ALLOWED_VALUES = [0, 1].freeze

    def initialize(matrix)
      @matrix = matrix
      perform_actions
    end

    def print_matrix
      puts @matrix.to_a.map(&:inspect)
    end

    private

    def perform_actions
      check_matrix_exists
      check_allowed_values
      solve_matrix
    end

    def check_matrix_exists
      raise StandardError.new("Error initializing object: Matrix can't be nil.") unless @matrix
    end

    def check_allowed_values
      unless (rows.flatten.uniq - ALLOWED_VALUES).empty?
        raise StandardError.new("Error validating row content: Passed Matrix has invalid values. Only #{ALLOWED_VALUES} are allowed.") unless rows.all? { |value| ALLOWED_VALUES.include? value }
      end
    end

    def solve_matrix
      width, height, start_index = nil, nil, nil
      zeroes_lines_info, halt_on_next = [], false
      rows.each_with_index do |row, index|
        zeroes_line_info = find_zeroes_line_info(row, index)
        if zeroes_line_info
          raise StandardError.new("Error while solving matrix: There is more than one zeroes rectangle on matrix.") if halt_on_next
          start_index = OpenStruct.new(x: zeroes_line_info.first_index, y: index) if zeroes_lines_info.empty?
          zeroes_lines_info << zeroes_line_info
        else
          halt_on_next = true unless zeroes_lines_info.empty?
        end
      end
      return if zeroes_lines_info.empty?
      raise StandardError.new("Error while validating result: Not all the lines of zeroes has the same size or its coordinates differs") if zeroes_lines_info.compact.uniq.size > 1
      first = zeroes_lines_info.first
      width = first.zeroes_line.size
      height = zeroes_lines_info.size
      @zeroes_rectangle = OpenStruct.new(width: first.zeroes_line.size, height: height, start_index: start_index)
    end

    def find_zeroes_line_info(array, index)
      first_index = first_index_of(array, 0)
      last_index = last_index_of(array, 0)
      return unless first_index && last_index
      zeroes_line = array[first_index..last_index]
      return if zeroes_line.empty?
      check_zeroes_line(zeroes_line, index)
      OpenStruct.new(zeroes_line: zeroes_line, first_index: first_index, last_index: last_index)
    end

    def check_zeroes_line(zeroes_line, index)
      raise StandardError.new("Error while validating row #{index}: There are non zero values between the first and the last zero.") unless zeroes_line.all? { |value| value == 0 }
    end

    def rows
      (0...@matrix.row_count).map { |row_index| row(row_index) }
    end

    def row(index)
      @matrix.row(index).to_a
    end

    def first_index_of(array, value)
      array.index(value)
    end

    def last_index_of(array, value)
      array.size - 1 - array.reverse.index(value) if array.reverse.index(value)
    end
  end
end
